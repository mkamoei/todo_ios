//
//  FacebookLoginViewController.swift
//  MyToDo
//
//  Created by Mateus Kamoei on 21/11/2017.
//  Copyright © 2017 Bruno Philipe. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class FacebookLoginViewController: UIViewController {

    let loginButton = FBSDKLoginButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Check for FBSDKAccessToken
        if (FBSDKAccessToken.current() != nil) {
            print("User is already logged in... go to next view controller")
            self.performSegue(withIdentifier: "logged_in_segue", sender: self)
        }

        // Adds and positions Facebook Login button on screen
        loginButton.center = self.view.center
        loginButton.delegate = self
        self.view.addSubview(loginButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// FBSDKLoginButtonDelegate
extension FacebookLoginViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if result.token != nil { // If login was successful go to next screen
            print("User logged in successfully")
            self.performSegue(withIdentifier: "logged_in_segue", sender: self)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
}
